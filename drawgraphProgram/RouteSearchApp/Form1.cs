﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GraphDrawing;
using System.Web;

namespace RouteSearchApp
{
    public partial class Form1 : Form
    {
        DrawGraph G;
        List<Vertex> V;
        List<Edge> E;
        int[,] AMatrix;
        Vertex BeginVert;
        int selFirst;
        int selSecound;
        public Form1()
        {
            InitializeComponent();
            V = new List<Vertex>();
            G = new DrawGraph(sheet.Width, sheet.Height);
            E = new List<Edge>();
            sheet.Image = G.GetBitmap();

        }
        // методы для алгоритма Дейкстры
        private Edge GetMyEdge(Vertex a, Vertex b)
        {
            IEnumerable<Edge> myedge = from edg in E where (edg.vert1 == a & edg.vert2 == b) || (edg.vert2 == a & edg.vert1 == b) select edg;
            if (myedge.Count() > 1 || myedge.Count() == 0)
            {
                return null;
            }
            else
            {
                return myedge.First();
            }
        }
        private Vertex GetAnotherUncheckedVert()
        {
            IEnumerable<Vertex> vertexesuncheck = from vert in V where vert.IsChecked == false select vert;
            if (vertexesuncheck.Count() != 0)
            {
                double minVal = vertexesuncheck.First().MarkValue;
                Vertex minPoint = vertexesuncheck.First();
                foreach (Vertex vert in vertexesuncheck)
                {
                    if (vert.MarkValue < minVal)
                    {
                        minVal = vert.MarkValue;
                        minPoint = vert;
                    }
                }
                return minPoint;
            }
            else
            {
                return null;
            }
        }
        private void OneStep(Vertex beginVert)
        {
            foreach (Vertex nextv in Prev(beginVert))
            {
                if (nextv.IsChecked == false)//не отмечена
                {
                    double newmark = beginVert.MarkValue + GetMyEdge(nextv, beginVert).Weight;
                    if (nextv.MarkValue > newmark)
                    {
                        nextv.MarkValue = newmark;
                        nextv.PrevVert = beginVert;
                    }
                    else
                    {

                    }
                }
            }
            beginVert.IsChecked = true;//вычеркиваем

        }
        private IEnumerable<Vertex> Prev(Vertex currVert)
        {
            IEnumerable<Vertex> firstVertexes = from fv in E where fv.vert1 == currVert select fv.vert2;
            IEnumerable<Vertex> secondVertexes = from sv in E where sv.vert2 == currVert select sv.vert1;
            IEnumerable<Vertex> totalpoints = firstVertexes.Concat<Vertex>(secondVertexes);
            return totalpoints;
        }
        private void AlgoritmRun(Vertex beginVert)
        {
            BeginVert = beginVert;
            OneStep(beginVert);
            foreach (Vertex vertex in V)
            {
                Vertex anotherP = GetAnotherUncheckedVert();
                if (anotherP != null)
                {
                    OneStep(anotherP);
                }
                else
                {
                    break;
                }

            }
        }
        public List<Vertex> MinPath1(Vertex end)
        {
            List<Vertex> listOfVertexes = new List<Vertex>();
            Vertex tempv = new Vertex(0, 0);
            tempv = end;
            bool flag = true;
            do
            {
                if (tempv == this.BeginVert) flag = false;
                if(tempv.PrevVert == null && tempv != this.BeginVert)
                {
                    G.DrawALLGraph(V, E);
                    MessageBox.Show("Путь между двумя вершинами отсутвует", "Отсутствие пути", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return null;                    
                }
                listOfVertexes.Add(tempv);
                tempv = tempv.PrevVert;
            }while (flag);

                return listOfVertexes;
        }
        private void DrawAllPath(List<Vertex> path)
        {
            for (int i = 0; i < path.Count - 1; i++)
            {
                G.DrawPath(GetMyEdge(path[i], path[i + 1]));
            }            
        }

        private void selectButton_Click(object sender, EventArgs e)
        {
            drawEdgeButton.Enabled = true;
            drawVertexButton.Enabled = true;
            deleteButton.Enabled = true;
            selectButton.Enabled = false;
            G.ClearSheet();
            G.DrawALLGraph(V, E);
            sheet.Image = G.GetBitmap();
            selFirst = -1;
            selSecound = -1;
        }

        private void drawVertexButton_Click(object sender, EventArgs e)
        {
            drawEdgeButton.Enabled = true;
            drawVertexButton.Enabled = false;
            deleteButton.Enabled = true;
            selectButton.Enabled = true;
            G.ClearSheet();
            G.DrawALLGraph(V, E);
            sheet.Image = G.GetBitmap();
        }

        private void drawEdgeButton_Click(object sender, EventArgs e)
        {
            drawEdgeButton.Enabled = false;
            drawVertexButton.Enabled = true;
            deleteButton.Enabled = true;
            selectButton.Enabled = true;
            G.ClearSheet();
            G.DrawALLGraph(V, E);
            sheet.Image = G.GetBitmap();
            selFirst = -1;
            selSecound = -1;
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            drawEdgeButton.Enabled = true;
            drawVertexButton.Enabled = true;
            deleteButton.Enabled = false;
            selectButton.Enabled = true;
            G.ClearSheet();
            G.DrawALLGraph(V, E);
            sheet.Image = G.GetBitmap();
        }

        private void sheet_MouseClick(object sender, MouseEventArgs e)
        {
            if (selectButton.Enabled == false)
            {
                if (e.Button == MouseButtons.Left)
                {
                    for (int i = 0; i < V.Count; i++)
                    {
                        if (Math.Pow((V[i].X - e.X), 2) + Math.Pow((V[i].Y - e.Y), 2) <= G.Radius * G.Radius)
                        {
                            if (selFirst == -1)
                            {
                                G.DrawALLGraph(V, E);
                                G.SelectVertex(V[i].X, V[i].Y);
                                selFirst = i;
                                V[selFirst].MarkValue = 0;
                                AlgoritmRun(V[selFirst]);
                                foreach (Vertex vert in V)
                                {
                                    vert.MarkValue = 1000000;
                                    vert.IsChecked = false;
                                }
                                sheet.Image = G.GetBitmap();
                                break;
                            }
                            if (selSecound == -1)
                            {
                                G.SelectVertex(V[i].X, V[i].Y);
                                selSecound = i;
                                if (V[selFirst] != V[selSecound])
                                {
                                    List<Vertex> path = MinPath1(V[selSecound]);
                                    if(path != null)
                                        DrawAllPath(MinPath1(V[selSecound]));                                    
                                    selFirst = -1;
                                    selSecound = -1;
                                }
                                else
                                {
                                    selSecound = -1;
                                }
                                sheet.Image = G.GetBitmap();
                                break;
                            }
                        }
                    }
                }
                if (e.Button == MouseButtons.Right)
                {
                    if ((selFirst != -1) &&
                        (Math.Pow((V[selFirst].X - e.X), 2) + Math.Pow((V[selFirst].Y - e.Y), 2) <= G.Radius * G.Radius))
                    {
                        G.DrawVertex(V[selFirst].X, V[selFirst].Y, (selFirst + 1).ToString());
                        selFirst = -1;
                        sheet.Image = G.GetBitmap();
                    }
                }
            }
            if (drawVertexButton.Enabled == false)
            {
                V.Add(new Vertex(e.X, e.Y));
                V[V.Count - 1].Number = V.Count;
                G.DrawVertex(e.X, e.Y, V[V.Count - 1].Number.ToString());
                sheet.Image = G.GetBitmap();
            }
            //нажата кнопка "рисовать ребро"
            if (drawEdgeButton.Enabled == false)
            {
                if (e.Button == MouseButtons.Left)
                {
                    for (int i = 0; i < V.Count; i++)
                    {
                        if (Math.Pow((V[i].X - e.X), 2) + Math.Pow((V[i].Y - e.Y), 2) <= G.Radius * G.Radius)
                        {
                            if (selFirst == -1)
                            {
                                G.SelectVertex(V[i].X, V[i].Y);
                                selFirst = i;
                                sheet.Image = G.GetBitmap();
                                break;
                            }
                            if (selSecound == -1)
                            {
                                G.SelectVertex(V[i].X, V[i].Y);
                                selSecound = i;
                                if (V[selFirst] != V[selSecound])
                                {
                                    E.Add(new Edge(selFirst, selSecound, V));
                                    G.DrawEdge(V[selFirst], V[selSecound], E[E.Count - 1]);
                                    selFirst = -1;
                                    selSecound = -1;
                                }
                                else
                                {
                                    selSecound = -1;
                                }
                                sheet.Image = G.GetBitmap();
                                break;
                            }
                        }
                    }
                }
                if (e.Button == MouseButtons.Right)
                {
                    if ((selFirst != -1) &&
                        (Math.Pow((V[selFirst].X - e.X), 2) + Math.Pow((V[selFirst].Y - e.Y), 2) <= G.Radius * G.Radius))
                    {
                        G.DrawVertex(V[selFirst].X, V[selFirst].Y, (selFirst + 1).ToString());
                        selFirst = -1;
                        sheet.Image = G.GetBitmap();
                    }
                }
            }
            //нажата кнопка "удалить элемент"
            if (deleteButton.Enabled == false)
            {
                bool flag = false; //удалили ли что-нибудь по ЭТОМУ клику
                //ищем, возможно была нажата вершина
                for (int i = 0; i < V.Count; i++)
                {
                    if (Math.Pow((V[i].X - e.X), 2) + Math.Pow((V[i].Y - e.Y), 2) <= G.Radius * G.Radius)
                    {
                        for (int j = 0; j < E.Count; j++)
                        {
                            if ((E[j].V1 == i) || (E[j].V2 == i))
                            {
                                E.RemoveAt(j);
                                j--;
                            }
                            else
                            {
                                if (E[j].V1 > i) E[j].V1--;
                                if (E[j].V2 > i) E[j].V2--;
                            }
                        }
                        V.RemoveAt(i);
                        flag = true;
                        break;
                    }
                }
                //ищем, возможно было нажато ребро
                if (!flag)
                {
                    for (int i = 0; i < E.Count; i++)
                    {
                        if (((e.X - V[E[i].V1].X) * (V[E[i].V2].Y - V[E[i].V1].Y) / (V[E[i].V2].X - V[E[i].V1].X) + V[E[i].V1].Y) <= (e.Y + 4) &&
                                ((e.X - V[E[i].V1].X) * (V[E[i].V2].Y - V[E[i].V1].Y) / (V[E[i].V2].X - V[E[i].V1].X) + V[E[i].V1].Y) >= (e.Y - 4))
                        {
                            if ((V[E[i].V1].X <= V[E[i].V2].X && V[E[i].V1].X <= e.X && e.X <= V[E[i].V2].X) ||
                                (V[E[i].V1].X >= V[E[i].V2].X && V[E[i].V1].X >= e.X && e.X >= V[E[i].V2].X))
                            {
                                E.RemoveAt(i);
                                flag = true;
                                break;
                            }
                        }
                    }
                }
                //если что-то было удалено, то обновляем граф на экране
                if (flag)
                {
                    G.ClearSheet();
                    G.DrawALLGraph(V, E);
                    sheet.Image = G.GetBitmap();
                }
            }
        }

        private void googleMaps_Click(object sender, EventArgs e)
        {
            GoogleForm f = new GoogleForm();            
            f.Show();
            this.Hide();
        }

        private void clearButton_Click(object sender, EventArgs e)
        {
            G.ClearSheet();
            V.Clear();
            E.Clear();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
