﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Web;
using Microsoft.Win32;

namespace RouteSearchApp
{
    public partial class GoogleForm : Form
    {
        public GoogleForm()
        {
            InitializeComponent();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            SetWebBrowserVersion(11001);
            cboGoogle.SelectedIndex = 0;
        }
        private void btnGoogle_Click(object sender, EventArgs e)
        {
            string url = GoogleMapUrl(txtAddress.Text, cboGoogle.Text, 0);
            wbrMap.Navigate(url);
        }
        private string GoogleMapUrl(string query, string map_type, int zoom)
        {
            string url = "http://maps.google.com/maps?";
            url += "q=" + HttpUtility.UrlEncode(query, Encoding.UTF8);
            map_type = GoogleMapTypeCode(map_type);
            if (map_type != null) url += "&t=" + map_type;
            if (zoom > 0) url += "&z=" + zoom.ToString();

            return url;
        }
        private string GoogleMapTypeCode(string map_type)
        {
            switch (map_type)
            {
                case "Map":
                    return "m";
                case "Satellite":
                    return "k";
                case "Hybrid":
                    return "h";
                case "Terrain":
                    return "p";
                case "Google Earth":
                    return "e";
                default:
                    return null;
            }
        }
        private void SetWebBrowserVersion(int ie_version)
        {
            const string key64bit =
                @"SOFTWARE\Wow6432Node\Microsoft\Internet Explorer\MAIN\FeatureControl\FEATURE_BROWSER_EMULATION";
            const string key32bit =
                @"SOFTWARE\Microsoft\Internet Explorer\MAIN\FeatureControl\FEATURE_BROWSER_EMULATION";
            string app_name = System.AppDomain.CurrentDomain.FriendlyName;
            SetRegistryDword(key32bit, app_name, ie_version);
        }

        // Set a registry DWORD value.
        private void SetRegistryDword(string key_name, string value_name, int value)
        {
            RegistryKey key =
                Registry.CurrentUser.OpenSubKey(key_name, true);
            if (key == null)
                key = Registry.CurrentUser.CreateSubKey(key_name,
                    RegistryKeyPermissionCheck.ReadWriteSubTree);
            key.SetValue(value_name, value, RegistryValueKind.DWord);

            key.Close();
        }

        // Delete a registry value.
        private void DeleteRegistryValue(string key_name, string value_name)
        {
            RegistryKey key =
                Registry.CurrentUser.OpenSubKey(key_name, true);
            if (key == null) return;            
            key.DeleteValue(value_name, false);

            key.Close();
        }

        private void graphBuilder_Click(object sender, EventArgs e)
        {
            Form1 f = new Form1();
            this.Hide();
            this.Dispose();
            f.Show();
        }
    }
}
