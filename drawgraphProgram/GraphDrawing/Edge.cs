﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphDrawing
{
    public class Edge
    {
        protected int v1;
        public Vertex vert1; 
        public int V1
        {
            set
            {
                v1 = value;
            }
            get
            {
                return v1;
            }
        }
        protected int v2;
        public Vertex vert2;
        public int V2
        {
            set
            {
                v2 = value;
            }
            get
            {
                return v2;
            }
        }
        protected double weight;
        public double Weight
        {
            set
            {
                weight = value;
            }
            get
            {
                return weight;
            }
        }
        public Edge(int V1, int V2, List<Vertex> vertexes)
        {
            this.V1 = V1;
            this.V2 = V2;
            vert1 = vertexes[v1];
            vert2 = vertexes[v2];            
        }

    }
}
