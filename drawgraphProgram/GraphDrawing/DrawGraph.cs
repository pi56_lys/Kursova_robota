﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace GraphDrawing
{
    public class DrawGraph
    {
        protected Bitmap bit;
        protected Pen blackPen;
        protected Pen redPen;
        protected Pen darkKhakiPen;
        protected Pen darkMagentaPen;
        protected Graphics g;
        protected Font font;
        protected Brush br;
        protected PointF point;
        protected int radius = 15;
        public int Radius
        {
            get
            {
                return radius;
            }
        }
        public void DrawString(float x, float y, float angle, string text)
        {
            Matrix original_transform = g.Transform.Clone();
            g.TranslateTransform(x, y);
            g.RotateTransform(angle);

            g.DrawString(text, font, br, 0, 0);

            g.Transform = original_transform;
        }
        public static double Angle(Vertex v1, Vertex v2)
        {
            double length = Math.Sqrt(Math.Pow(v2.X - v1.X, 2) + Math.Pow(v2.Y - v1.Y, 2));
            double angle;
            angle = Math.Acos((v2.X - v1.X) / length) / Math.PI * 180;
            if (v2.X - v1.X < 0) angle += 180;
            if (v2.Y - v1.Y < 0) angle = - angle; 
            return angle;
        }
        public DrawGraph(int width, int height)
        {
            bit = new Bitmap(width, height);
            g = Graphics.FromImage(bit);
            ClearSheet();
            blackPen = new Pen(Color.Black, 2);
            redPen = new Pen(Color.Red, 2);
            darkMagentaPen = new Pen(Color.DarkMagenta, 2);
            darkKhakiPen = new Pen(Color.DarkKhaki, 2);
            font = new Font("Times New Roman", 12);
            br = Brushes.Black;
        }
        public void SelectVertex(int x, int y)
        {
            g.DrawEllipse(redPen, x - radius, y - radius, 2 * radius, 2 * radius);
        }
        public void DrawPath(Edge edge)
        {
            g.DrawLine(darkKhakiPen, edge.vert1.X, edge.vert1.Y, edge.vert2.X, edge.vert2.Y);
            DrawString((edge.vert1.X + edge.vert2.X) / 2f + 1f, (edge.vert1.Y + edge.vert2.Y) / 2f + 1f, (float)Angle(edge.vert1, edge.vert2), edge.Weight.ToString());
            g.FillEllipse(Brushes.White, (edge.vert1.X - radius), (edge.vert1.Y - radius), 2 * radius, 2 * radius);
            g.DrawEllipse(darkKhakiPen, (edge.vert1.X - radius), (edge.vert1.Y - radius), 2 * radius, 2 * radius);
            g.FillEllipse(Brushes.White, (edge.vert2.X - radius), (edge.vert2.Y - radius), 2 * radius, 2 * radius);
            g.DrawEllipse(darkKhakiPen, (edge.vert2.X - radius), (edge.vert2.Y - radius), 2 * radius, 2 * radius);
            g.DrawString(edge.vert2.Number.ToString(), font, br, edge.vert2.X - 7, edge.vert2.Y - 7);
            g.DrawString(edge.vert1.Number.ToString(), font, br, edge.vert1.X - 7, edge.vert1.Y - 7);
        }        
        public void DrawEdge(Vertex v1, Vertex v2, Edge e)
        {
            g.DrawLine(darkMagentaPen, v1.X, v1.Y, v2.X, v2.Y);
            e.Weight = Math.Round(Math.Sqrt(Math.Pow(v1.X - v2.X, 2) + Math.Pow(v1.Y - v2.Y, 2)),2);
            DrawString((v1.X + v2.X) / 2f + 1f, (v1.Y + v2.Y) / 2f + 1f, (float)Angle(v1, v2), e.Weight.ToString());
            DrawVertex(v1.X, v1.Y, (e.V1 + 1).ToString());
            DrawVertex(v2.X, v2.Y, (e.V2 + 1).ToString());
        }
        public void DrawVertex(int x, int y, string number)
        {
            g.FillEllipse(Brushes.White, (x - radius), (y - radius), 2 * radius, 2 * radius);
            g.DrawEllipse(blackPen, (x - radius), (y - radius), 2 * radius, 2 * radius);
            point = new PointF(x - 7, y - 7);
            g.DrawString(number, font, br, point);
        }
        public void DrawALLGraph(List<Vertex> V, List<Edge> E)
        {
            for (int i = 0; i < E.Count; i++)
            {
                g.DrawLine(darkMagentaPen, V[E[i].V1].X, V[E[i].V1].Y, V[E[i].V2].X, V[E[i].V2].Y);
                float x, y;
                x = (V[E[i].V1].X + V[E[i].V2].X) / 2f + 1f;
                y = (V[E[i].V1].Y + V[E[i].V2].Y) / 2f + 1f;
                DrawString(x, y, (float)Angle(V[E[i].V1], V[E[i].V2]), E[i].Weight.ToString());
            }            
            for (int i = 0; i < V.Count; i++)
            {
                DrawVertex(V[i].X + 1, V[i].Y, (i + 1).ToString());
            }
        }

        public void ClearSheet()
        {
            g.Clear(Color.White);
        }

        public Bitmap GetBitmap()
        {
            return bit;
        }
    }
}
