﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphDrawing
{
    public class Vertex
    {
        public int Number { get; set; }
        public double MarkValue { get; set; }
        public bool IsChecked { get; set; }
        public Vertex PrevVert { get; set; }
        protected int x; 
        public int X
        {
            get
            {
                return x;
            }
            set
            {
                x = value; 
            }
        }
        protected int y;
        public int Y
        {
            get
            {
                return y;
            }
            set
            {
                y = value;
            }
        }
        public Vertex(int X, int Y)
        {
            this.X = X;
            this.Y = Y;
            this.IsChecked = false;
            this.MarkValue = 1000000;
        }
    }
}
